package org.lucky.springboot.rest.controller;

import org.lucky.springboot.rest.model.AuthenticationRequest;
import org.lucky.springboot.rest.model.AuthenticationResponse;
import org.lucky.springboot.rest.model.UserDetails;
import org.lucky.springboot.rest.service.MyUserDetailService;
import org.lucky.springboot.rest.utils.JwtUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Arrays;
import java.util.Map;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    Logger logger = LoggerFactory.getLogger(RestController.class);
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @Autowired
    private MyUserDetailService userDetailsService;

    @GetMapping("/get")
    public ResponseEntity getCall(){

        logger.info("getCall started....!");
        LinkedMultiValueMap<Object, Object> valueMap = new LinkedMultiValueMap<>();
        valueMap.putAll(Map.of(
                "header1", Arrays.asList("value1"),
                "header2", Arrays.asList("value2"),
                "header3", Arrays.asList("value3")
        ));
        return new ResponseEntity(
                Map.of(
                        "key1",Map.of("innerKey1","innerValue1"),
                        "key2","value2",
                        "key3","value3",
                        "user", UserDetails.builder()
                                .name("lucky")
                                .password(passwordEncrypt("password"))
                                .build()
                ), valueMap, HttpStatus.OK
        );
    }

    @PostMapping(value = "/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
            );
        }
        catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }

        final org.springframework.security.core.userdetails.UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        final String jwt = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(AuthenticationResponse.builder().jwt(jwt).build());
    }

    public String passwordEncrypt(String original){

    return new String(
                 new char[original.length()]
             )
            .replace('\0', '*');
    }
}
