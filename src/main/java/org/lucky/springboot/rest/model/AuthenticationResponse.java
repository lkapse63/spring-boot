package org.lucky.springboot.rest.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
@Getter
@Setter
@Builder
public class AuthenticationResponse implements Serializable {
    final private  String jwt;
}
