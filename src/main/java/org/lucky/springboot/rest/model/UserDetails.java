package org.lucky.springboot.rest.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserDetails {

    private String name;
    private String password;
}
