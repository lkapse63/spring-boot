package org.lucky.springboot.rest.service;

import org.lucky.springboot.rest.model.MyUserDetails;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailService implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return MyUserDetails.builder()
                .myActive(true)
                .myUserName(s)
                .myPassword(s)
                .myRoles("ROLE_USER")
                .build();
    }
}
