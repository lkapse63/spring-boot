package org.lucky.springboot.rest.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RestControllerTest {

    private RestController controller;

    @BeforeEach
    void init() throws Exception{
        controller =  new RestController();
    }

    @Test
    void passwordEncrypt() {
        String password = controller.passwordEncrypt("password");
        assertEquals(password.length(),"password".length());
    }
}